<?php
namespace eymfw\base;

/**
 * Action is a framework base action directly extending yii's \yii\base\Action
 * The class implements eymfw\base\IBaseAction interface methods beforeExecute,execute and afterExecute
 *
 * @Author: Nino Gabon
 */ 
class Action extends \yii\base\Action implements \eymfw\base\IAction{

    /**
     * @event Event raised on beforeExecute
     */
	const EVENT_BEFORE_EXEC = 'beforeExecute';
	
    /**
     * @event Event raised on execute
     */	
	const EVENT_ON_EXEC = 'execute';
	
    /**
     * @event Event raised on afterExecute 
     */	
	const EVENT_AFTER_EXEC = 'afterExecute';

    /**
     * beforeExecute is run to check whether or not to continue to the Event::execute method
	 * triggers a self::EVENT_BEFORE_EXEC event to allow behaviors or filters to run 
     * @return boolean whether to run Event::execute or not 
     */		
	function beforeExecute()
	{
		$event=new \eymfw\base\Event();
		$this->trigger(self::EVENT_BEFORE_EXEC,$event);
		return true;
	}
	
    /**
     * execute contains the procedures relevant to the action class
	 * triggers a self::EVENT_ON_EXEC event to allow behaviors or filters to run
     * @param mixed $results passed to the method, defaults to null 
     * @return mixed event content after a series of procedures have been run and may also
     * represent information after a series of behaviors or filters have been run	 
     */			
	function execute($results=null)
	{
		$event=new \eymfw\base\Event();
		if(isset($results)){
			$event->content=$results;
		}
		$this->trigger(self::EVENT_ON_EXEC, $event);
		return $event->content;
	}
	
    /**
     * afterExecute contains the procedures that must be run after the main execute method
	 * triggers a self::EVENT_AFTER_EXEC event to allow behaviors or filters to run
     * @param mixed $results from Event::execute, defaults to null 
     * @return mixed event content after a series of procedures have been run and may also
     * represent information after a series of behaviors or filters have been run	
     */		
	function afterExecute($results=null)
	{
		$event=new \eymfw\base\Event();
		if(isset($results)){
			$event->content=$results;
		}		
		$this->trigger(self::EVENT_AFTER_EXEC, $event);
		return $event->content;
	}
	
    /**
     * runs beforeExecute,execute and afterExecute
	 * execute method only runs when beforeExecute returns true
     * this method must be overriden if there is an expected parameter to be passed to the execute method	 
     * @returns mixed results obtained from Event::afterExecute 
     */		
	function run()
	{
		
		$results = null;
		
		if
		( $this->beforeExecute() )
		{
			$results=$this->afterExecute($this->execute($results));			
		}
		
		return 	$results; 
		
	}
	
}
?>