<?php
namespace eymfw\base; 

/**
 * ActiveRecord base active record class for the eymfw libraries.
 * The class extends from \yii\db\ActiveRecord so it behaves similar to the ActiveRecord class.
 * Additional behavior is that it overrides the parent's implementation of the tableName() method.
 * @Author: Nino Gabon
 */ 
class ActiveRecord extends \yii\db\ActiveRecord{
	
	/**
	 * @returns string the default table name representing a database table of similar name.
	 */
    public static function tableName()
    {
    	return \yii\helpers\StringHelper::basename(get_called_class());
    }	
}
?>