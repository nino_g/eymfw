<?php
namespace eymfw\base; 

/**
 * @author: Nino Gabon
 */ 
class BaseAction extends \eymfw\base\Action{

	public $documentType;
	
	public $package;
	
	/**
	 * @var Description of the action class
	 */	
	public $description;
	
	public $defaultRoles;
	
	public $modelClass;
	
	
	function execute($results=null)
	{								
		if(isset($_GET['event'])){
			$event=new \eymfw\base\Event();
			if(isset($results)){
				$event->content=$results;
			}
			$this->trigger('before'.\yii\helpers\Inflector::camelize($_GET['event']), $event);
			$this->trigger($_GET['event'], $event);
			$this->trigger('after'.\yii\helpers\Inflector::camelize($_GET['event']), $event);
			$results = $event->content;
		}
        
		return parent::execute($results);
	}		
	
}
?>