<?php
namespace eymfw\base;

/**
 * @author: Nino Gabon
 */ 
class BaseViewAction extends \eymfw\base\BaseAction{

	/**
	 * @var View alias file name to render when afterExecute is run
	 */
	public $view;
	
	/**
	 * @var Layout name for the view to render
	 */
	public $layout;
				
}
?>