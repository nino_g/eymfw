<?php
namespace eymfw\base; 

use eymfw\base\IController;

/**
 * BaseController directly extends from \yii\web\Controller and implements 
 * eymfw\base\IBaseController interface methods setModel and getModel
 * The controller is used mainly to encapsulate the model 
 *
 * @Author: Nino Gabon
 */
class Controller extends \yii\web\Controller implements IController{

    /**
     * @var Model encapsulates related data
     */
	private $model=null;
	
    /**
     * sets the model for the controller
	 * @param Model to encapsulate the data
     */		
    function setModel($model){
    	$this->model=$model;
    }
	
    /**
     * gets the model for the controller
	 * @param Model to set for the controller when its model is empty or null.
	 * @returns Model that encapsulates data
     */		
    function getModel($model=null){
    	if(!isset($this->model)){
    		$this->model=$model;
    	}
    	return $this->model;	
    }	
}
?>