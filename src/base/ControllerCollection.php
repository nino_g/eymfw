<?php
namespace eymfw\base;

use yii\base\Component;
use yii\base\InvalidParamException;
use Yii;

/**
 * Collection is a storage for all controllers/actions and behaviors in the application.
 *
 * @author Nino Gabon <ngabon@excelym.com>
 * @since 2.0
 */
class ControllerCollection extends Component
{
    /**
     * @var string constant collection is
     */
	const COLLECTION_ID = "HTCtrlCol2017-zvhT12pil0mewzt" ; 

    /**
     * @var array list of Auth clients with their configuration in format: 'clientId' => [...]
     */
    private $_controllers = [];
	
	public function init(){
		$cache = \yii::$app->cache->get(\eymfw\base\ControllerCollection::COLLECTION_ID);
		if(isset($cache['controllers'])){
			$this->_controllers = $cache['controllers'];			
		}	
	}

    /**
     * @param array $controllers list of preferences/task objects
     */
    public function setControllers(array $controllers)
    {
        $this->_controllers = $controllers;
    }

    /**
     * @return array list of controllers.
     */
    public function getControllers()
    {	
        return $this->_controllers;
    }
	
    /**
     * add a list of controllers.
     */
    public function addControllers($controllers)
    {	
        $this->_controllers = array_merge($this->_controllers,$controllers);
		\yii::$app->cache->set(\eymfw\base\ControllerCollection::COLLECTION_ID,['controllers'=>$this->_controllers],3600);
    }	

    /**
     * Checks if client exists in the hub.
     * @param string $id client id.
     * @return bool whether client exist.
     */
    public function hasController($id)
    {
        return array_key_exists($id, $this->_controllers);
    }

}