<?php
namespace eymfw\base;

/**
 *@Author: Nino Gabon
 */
class Event extends \yii\base\Event {

	/** 
	* @var content An array of all contents to be sent to the response object.
	* Set the content that will be sent to the response object in every event handler.
	*/
	public $content;
	
}
?>