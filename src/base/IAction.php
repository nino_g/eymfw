<?php
namespace eymfw\base;

/**
 * IAction base action interface for the eymfw libraries.
 * Action classes under the eymfw library must implement beforeExecute, execute & afterExecute methods.
 * @Author: Nino Gabon
 */ 
interface IAction{
	/**
	 * beforeExecute must @return boolean when implemented
	 */
    function beforeExecute();
	
	/**
	 * execute must only run when beforeExecute returns true.
     * @param $results mixed	 
	 */
    function execute($results=null);
	
	/**
	 * afterExecute must execute right after the execute method is run.
	 * @params $results mixed 
	 */
    function afterExecute($results=null);
}
?>