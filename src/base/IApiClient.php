<?php
namespace eymfw\base;

/**
 * IApiClient base interface for the eymfw libraries representing an api client.
 * Api classes under the eymfw library must implement call, getResponseHeaders methods.
 * @Author: Nino Gabon
 */
interface IApiClient{
	/**
	 * call method to invoke an api endpoint
	 * @params string the method type e.g. GET,POST,PUT
	 * @params string the endpoint to make the call e.g. admin/orders 
	 * @params array the payload to send
	 */
	function call($method, $path, $params=array());
	
	/**
	 * getResponseHeaders 
	 * @returns array response headers received after making an api call
	 */
	function getResponseHeaders();
}
?>