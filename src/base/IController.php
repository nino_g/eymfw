<?php
namespace eymfw\base;

/**
 * IController base controller interface for the eymfw libraries.
 * Controller classes under the xyif library must implement setModel, & getModel methods.
 * @Author: Nino Gabon
 */ 
interface IController{
	/**
	 * setModel requires @params $model to encapsulate iformation relevant to the current controller
	 */
    function setModel($model);
	
	/**
	 * getModel @returns current $model relevant to the current controller
	 */
    function getModel($model=null);
}
?>