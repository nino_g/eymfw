<?php
namespace eymfw\base;

use yii\base\Component;
use yii\base\InvalidParamException;
use Yii;

/**
 * Preference is a storage for all controllers/actions, behaviors, documents & other preferences in the application.
 *
 * @author Nino Gabon <ngabon@excelym.com>
 * @since 2.0
 */
class PreferenceCollection extends Component
{
    /**
     * @var string constant collection is
     */
	const COLLECTION_ID = "HTCtrlCol2017-zvhT12pil0mewzt" ; 

    /**
     * @var array list of preferences with their configuration in format: 'preferenceId' => [...]
     */
    private $_preferences = [];
	
	public function init(){
		$cache = \yii::$app->cache->get(\eymfw\base\PreferenceCollection::COLLECTION_ID);
		if(isset($cache['preferences'])){
			$this->_preferences = $cache['preferences'];			
		}	
	}

    /**
     * @param array $preferences list of preferences/task objects
     */
    public function setPreferences($preferences=[])
    {
        $this->_preferences = $preferences;
    }

    /**
     * @return array list of preferences.
     */
    public function getPreferences()
    {	
        return $this->_preferences;
    }
	
    /**
     * add a list of preferences.
     */
    public function addPreferences($preferences)
    {	
        $this->_preferences = array_merge($this->_preferences,$preferences);
		\yii::$app->cache->set(\eymfw\base\PreferenceCollection::COLLECTION_ID,['preferences'=>$this->_preferences],3600);
    }	

    /**
     * Checks if a preference id exists in the hub.
     * @param string $id preference id.
     * @return bool whether a preference exist.
     */
    public function hasPreference($id)
    {
        return array_key_exists($id, $this->_preferences);
    }

}