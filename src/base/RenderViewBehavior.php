<?php
namespace eymfw\base;

/*
 *@Author: Nino Gabon
 */ 
class RenderViewBehavior extends \yii\base\Behavior{
	
    public function events()
    {
        return [
            \eymfw\base\Action::EVENT_AFTER_EXEC => 'afterExecute',
        ];
    }
    				          
    public function afterExecute($event)
    {
		$model=$event->sender->controller->getModel();
		
		if(isset($event->sender->layout)){
			$event->sender->controller->layout = $event->sender->layout;
		}		
		
		$event->content = $event->sender->controller->render($event->sender->view,
			['model'=>$model, 'pkg'=>$event->sender->package, 'doc_type'=>$event->sender->document_type]);
    }					
}
?>