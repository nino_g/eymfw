<?php
namespace eymfw\oauth2;


class Pdo extends \filsh\yii2\oauth2server\storage\Pdo
{    
    public function __construct($connection = null, $config = array())
    {		        
        parent::__construct($connection, $config);
        
        $this->config = array_merge(array(
            'client_table' => 'n_oauth_clients',
            'access_token_table' => 'n_oauth_access_tokens',
            'refresh_token_table' => 'n_oauth_refresh_tokens',
            'code_table' => 'n_oauth_authorization_codes',
            'user_table' => 'n_user',
            'jwt_table'  => 'n_oauth_jwt',
            'jti_table'  => 'n_oauth_jti',
            'scope_table'  => 'n_oauth_scopes',
            'public_key_table'  => 'n_oauth_public_keys',
        ), $config);    
    }
    
    public function getUser($username)
    {
        $stmt = $this->db->prepare($sql = sprintf('SELECT * from %s where email=:username', $this->config['user_table']));
        $stmt->execute(array('username' => $username));

        if (!$userInfo = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            return false;
        }

        // the default behavior is to use "username" as the user_id
        return array_merge(array(
            'user_id' => $username
        ), $userInfo);
    } 

    public function setUser($username, $password, $firstName = null, $lastName = null)
    {
        // do not store in plaintext
        $password = $this->hashPassword($password);

        // if it exists, update it.
        if ($this->getUser($username)) {
            $stmt = $this->db->prepare($sql = sprintf('UPDATE %s SET pwd=:password, re_pwd=:password, first_name=:firstName, last_name=:lastName where email=:username', $this->config['user_table']));
        } else {
            $stmt = $this->db->prepare(sprintf('INSERT INTO %s (email, pwd, ,re_pwd, first_name, last_name) VALUES (:username, :password, :password, :firstName, :lastName)', $this->config['user_table']));
        }

        return $stmt->execute(compact('email', 'pwd', 'firstName', 'lastName'));
    }    
       
    
}