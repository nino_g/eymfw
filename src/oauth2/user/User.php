<?php
/*
 *@Author: Nino Gabon
 */
namespace eymfw\oauth2\user; 

use yii\db\ActiveRecord;

class User extends ActiveRecord implements \yii\web\IdentityInterface, \OAuth2\Storage\UserCredentialsInterface{
 
    public $document_type='usr';
 
    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return 'n_user';
    }
  
    public function quickSearchFields()
    {
        return array('email', 'id', 'last_name', 'first_name');
    }
 
    public function rules() 
    {		
        return array_merge(parent::rules(), array(
                [['email','first_name','last_name','pwd','re_pwd'], 'required'],
                ['pwd', 'compare', 'compareAttribute'=>'re_pwd', 'operator'=>'=='],
                ['email', 'email'],
				['email', 'unique', 'targetAttribute' => ['email']]
            ));			
    }
	
	public function attributeLabels(){
		return  [
			'mi' => 'Middle Initial',
			'pwd' => 'Password',
			're_pwd' => 'Re-Password'
		];
	}
 
     
    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }
	
	public function getDescription(){
		if(!$this->getIsNewRecord()){					
			return $this->last_name.', '.$this->first_name;
		}
	}

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return boolean if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    } 
    
    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return password_verify($password,$this->pwd);
    }    
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = \Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return false;
    }    

    /**
     * Grant access tokens for basic user credentials.
     *
     * Check the supplied username and password for validity.
     *
     * You can also use the $client_id param to do any checks required based
     * on a client, if you need that.
     *
     * Required for OAuth2::GRANT_TYPE_USER_CREDENTIALS.
     *
     * @param $username
     * Username to be check with.
     * @param $password
     * Password to be check with.
     *
     * @return
     * TRUE if the username and password are valid, and FALSE if it isn't.
     * Moreover, if the username and password are valid, and you want to
     *
     * @see http://tools.ietf.org/html/rfc6749#section-4.3
     *
     * @ingroup oauth2_section_4
     */	
	public function checkUserCredentials($username, $password){
		
        $user = static::findOne(['email' => $username]);
        if (empty($user)) {
            return false;
        }
        return $user->validatePassword($password);		
		
	}
    
    /**
     * @return
     * ARRAY the associated "user_id" and optional "scope" values
     * This function MUST return FALSE if the requested user does not exist or is
     * invalid. "scope" is a space-separated list of restricted scopes.
     * @code
     * return array(
     *     "user_id"  => USER_ID,    // REQUIRED user_id to be stored with the authorization code or access token
     *     "scope"    => SCOPE       // OPTIONAL space-separated list of restricted scopes
     * );
     * @endcode
     */
    public function getUserDetails($username){
        $user = static::findOne(['email' => $username]);
        return ['user_id' => $user->id];		
	}		
	
    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
    	
        /** @var \filsh\yii2\oauth2server\Module $module */
        $module = \yii::$app->getModule('oauth2');
        $token = $module->getServer()->getResourceController()->getToken();
        return !empty($token['user_id'])
                    ? static::findIdentity($token['user_id'])
                    : null;    	
    			         
    }	
	
	
}
 
?>